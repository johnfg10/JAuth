package impl

import (
	"github.com/lestrrat-go/jwx/jwa"
	"crypto/rsa"
	"github.com/lestrrat-go/jwx/jwe"
)

type JWEConfig struct {
	KeyEncryptionAlgorithm jwa.KeyEncryptionAlgorithm
	ContentEncryptionAlgorithm jwa.ContentEncryptionAlgorithm
	CompressionAlgorithm jwa.CompressionAlgorithm

	Key []byte
	privateKey rsa.PrivateKey
}

func (config JWEConfig)EncodeData(value interface{}, params map[string]interface{}) ([]byte, error) {
	return jwe.Encrypt(value.([]byte), config.KeyEncryptionAlgorithm, config.Key, config.ContentEncryptionAlgorithm, config.CompressionAlgorithm)
}

func (config JWEConfig)DecodeData(bytes []byte, params map[string]interface{}) (interface{}, error) {
	return jwe.Decrypt(bytes, config.KeyEncryptionAlgorithm, config.Key)
}