package impl

import (
	"github.com/lestrrat-go/jwx/jwt"
	"github.com/lestrrat-go/jwx/jwa"
	"strings"
)

type JWTConfig struct {
	encodingMethod jwa.SignatureAlgorithm
	encodingKey []byte
}

func (config JWTConfig)EncodeData(value interface{}, params map[string]interface{}) ([]byte, error) {
	token := jwt.New()

	for k, v := range params {
		if k != "config" {
			token.Set(k, v)
		}
	}
	if config.encodingMethod == jwa.NoSignature {
		return token.MarshalJSON()
	}
	return token.Sign(config.encodingMethod, config.encodingKey)
}

//returns a *jwt.Token
func (config JWTConfig)DecodeData(value interface{}, params map[string]interface{}) (interface{}, error) {
	return jwt.ParseVerify(strings.NewReader(value.(string)), config.encodingMethod, config.encodingKey)
}
