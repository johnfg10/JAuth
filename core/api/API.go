package api

//for use with storing data for example in cookies
type JWXEncoder interface {
	EncodeData(value interface{}, params map[string]interface{}) ([]byte, error)

	DecodeData(bytes []byte, params map[string]interface{}) (interface{}, error)
}