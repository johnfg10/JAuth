package core

import (
	"gitlab.com/johnfg10/AuthN/core/api"
	"net/http"
	"strings"
	"log"
	"context"
)

func AuthBearerExtractor(authHeader string) string {
	return strings.Replace(authHeader, "bearer ", "", 1)
}

type AuthCoreConfig struct {
	encoder api.JWXEncoder
}

func (config *AuthCoreConfig)handler(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authHeader := r.Header.Get("Authorization")
		decodedData, err := config.encoder.DecodeData([]byte(authHeader), nil)
		if err != nil {
			log.Println(err)
		}
		context.WithValue(r.Context(), "token", decodedData)
		handler.ServeHTTP(w, r)
	})
}